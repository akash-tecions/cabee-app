import React, { Component } from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';



//import Map from "./src/page/Map";
import Index from './src/Index';
//import Search from "./src/Pages/Search";
//import Rating from "./src/Pages/Rating";
//import ZIndexMarkers from "./src/Pages/ZIndexMarkers";
import Form from "./src/Pages/Form";
import First from "./src/page/First";
import Image from "./src/Pages/Image";
import Card from "./src/Screen/Card";
import Places from "./src/Pages/Places";
import SearchScreen from "./src/Screen/SearchScreen";
import MovingCar from "./src/Pages/MovingCar";
import DriverCar from "./src/Screen/DriverCar";
import SearchPlaces from "./src/Pages/SearchPlaces";
//import Directions from "./src/Pages/PolylineCreator";
import GradientPolylines from "./src/Pages/GradientPolylines";
import Test from "./src/page/Test";
import Booking from "./src/Screen/Booking";
import AnimatedMarkers from "./src/Pages/AnimatedMarkers";
import Location from "./src/Pages/Location";
import Payment from "./src/Pages/Payment";
export default class App extends Component {
  render() {
    return (
        <View style={styles.container}>
          <View style={styles.bottom}>
            <StatusBar backgroundColor="#ffbf00" barStyle="light-content"/>

          <Index/>


          </View>
        </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  bottom: {
    height: '20%',
    backgroundColor: '#ffcf33',
    flexGrow: 1
  }
});