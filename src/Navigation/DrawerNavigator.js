import React from 'react';
import {Platform, Dimensions } from 'react-native';
import { createDrawerNavigator, createAppContainer } from 'react-navigation';

import YourTrips from '../Screen/YourTrips';
import Payment from "../Pages/Payment";
import SearchScreen from "../Screen/SearchScreen";

import MenuDrawer from '../Component/MenuDrawer';

const WIDTH = Dimensions.get('window').width;
const DrawerConfig = {
    drawerWidth: WIDTH*0.83,
  contentComponent: ({ navigation }) => {
      return (<MenuDrawer navigation={navigation}/>)
  }
}

const DrawerNavigator = createDrawerNavigator(
    {
        SearchScreen: {
        screen: SearchScreen
    },
    YourTrips: {
        screen: YourTrips
    },
        Payment: {
            screen: Payment,
        },
    },
    DrawerConfig
);
export default createAppContainer(DrawerNavigator);