import React, { Component } from 'react';
import {StyleSheet, View} from 'react-native';

import CodeInput from 'react-native-confirmation-code-field';

import Timer from '../Component/Timer';
import Form2 from '../Component/Form2';

export default class Second extends Component {
    render() {
        return (
            <View style={styles.container}>
            <Timer/>
            <Form2/>
            </View>
        )
    }
    handlerOnFulfill = code => console.log(code);

     render() {
         return <CodeInput onFulfill={this.handlerOnFulfill} />;
     }
}
const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});