import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
export default class Third extends Component {
    render() {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Home</Text>
        </View>
    );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize:30,
    }
});


