import React, { Component } from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';


import Logo from '../Component/Logo';
import Form from '../Component/Form';
export default class First extends Component {
    render() {
        return (
            <View style={styles.container}>
<Logo/>
<Form/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container : {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});