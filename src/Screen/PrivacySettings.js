import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";

export default class SavedPlace extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topLinks}>
                    <Text style={styles.priText}>Privacy settings</Text>
                </View>
                <Text style={styles.locText}>Location</Text>
                <Text style={styles.updText}>Update your location sharing preferences</Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    topLinks: {
        height: 50,
        backgroundColor: '#ffcf33'
    },
    priText: {
        fontSize:30
    },
    locText: {
        flex: 1,
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical: 25,
    },
    updText: {
        flex: 2,
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray',
        marginTop: 16
    }
});
