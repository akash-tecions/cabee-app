import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity } from 'react-native';

import BackButton from "../Pages/BackButton";

export default class AddPassword extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topLinks}>
                    <BackButton navigation={this.props.navigation} />
                    <Text style={styles.pwdText}>Add password</Text>
                </View>
                <Text style={styles.creText}>To add a new payment method, create a password for your Cabee account.</Text>
                <TextInput style={styles.inputBox}
                           keyboardType='numeric'
                           underlinecolorAndroid='black'
                           placeholder=" Minimum 8 characters"
                           placeholderTextColor="white"
                           secureTextEntry={true}
                />
                <TouchableOpacity style={styles.button} onPress={this.onPress}>
                    <Text style={styles.nextText}>Next</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    topLinks: {
        height: 80,
        backgroundColor: '#ffcf33'
    },
    pwdText: {
        fontSize: 30
    },
    creText: {
        textAlign: 'left',
        marginLeft: 20,
        marginRight:25,
        color: 'black',
        marginVertical: 20,
        fontSize: 20
    },
    inputBox: {
        width: 300,
        backgroundColor: 'gray',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        marginVertical: 16,
        marginLeft: 30,
    },
    button: {
        backgroundColor: '#ffcf33',
        borderRadius: 25,
        marginVertical: 30,
        width: 100,
        paddingVertical: 5,
        marginLeft: 130,

    },
    nextText: {
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 20,
        fontWeight: '500',
        color: 'black'
    },
});
