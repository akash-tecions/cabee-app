import React, {Component} from 'react';
import {Dimensions, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const WIDTH = Dimensions.get('window').width;

 const DestButton = function (props) {
    return(
        <TouchableOpacity onPress={() => {}} style={styles.container}>
            <View style={styles.leftCol}>
                <Text style={{fontSize: 10}}>{'\u25A0'}</Text>
            </View>

            <View style={styles.centerCol}>
                <Text style={{fontFamily: 'san-serif-thin', fontSize: 21, color: '#545454'}}>
                    Where to?
                </Text>
            </View>
            <View style={styles.rightCol}>
                <Icon name="car" backgroundColor="#3b5998" style={styles.carIcon}/>
            </View>

        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    container: {
        zIndex: 9,
        position: 'absolute',
        flexDirection: 'row',
        width: (WIDTH-40),
        height: 60,
        top: 110,
        left: 20,
        borderRadius: 2,
        backgroundColor: 'white',
        alignItems: 'center',
        shadowColor: '#000000',
        elevation: 7,
        shadowRadius: 5,
        shadowOpacity: 1.0
    },
    leftCol: {
        flex: 1,
        alignItems: 'center'
  },
    centerCol: {
        flex: 4
    },
    rightCol: {
        flex: 1,
        borderLeftWidth: 1,
        borderColor: '#ededed'
    },
    carIcon: {
      fontSize: 25
    }
})
export default DestButton;