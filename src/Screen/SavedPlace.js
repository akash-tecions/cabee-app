import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";
import BackButton from "../Pages/BackButton";

export default class SavedPlace extends Component {
    render() {
        return (
            <View style={styles.container}>
            <View style={styles.topLinks}>
                <BackButton navigation={this.props.navigation} />

                <Text style={styles.saveText}>Saved places</Text>
            </View>
                <Text style={styles.favText}>Favourites</Text>
                <Text style={styles.homeText}>Add Home</Text>
                <Text style={styles.workText}>Add Work</Text>
                <View style={styles.bottomLinks}>
                </View>
                <Text style={styles.addText}>Add saved place</Text>
                <Text style={styles.desText}>Get to your favourite destinations faster</Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    topLinks: {
        height: 80,
        backgroundColor: '#ffcf33'
    },
    saveText: {
        fontSize:30
    },
    favText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray',
        marginVertical:16
    },
    homeText: {
        textAlign: 'left',
        marginLeft: 30,
        color: 'black',
        marginHorizontal: 16
    },
    workText: {
        textAlign: 'left',
        marginLeft: 30,
        color: 'black',
        marginVertical:20
    },
    bottomLinks:{
        paddingTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#777777',
    },
    addText: {
        textAlign: 'left',
        marginLeft: 20,
        color: '#7AD7FD',
        marginVertical:16
    },
    desText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray'
    }
});
