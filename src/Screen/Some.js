import React, { Component } from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image, TextInput} from 'react-native';

import BackButton from "../Pages/BackButton";

export default class Some extends Component {
    render() {
        return (
            <View style={styles.container}>
                <BackButton navigation={this.props.navigation} />
                <Text style={styles.doText}>Do you need someone to drive for you?</Text>
                <TouchableOpacity style={styles.button} onPress={this.onPress}>
                    <Text style={styles.buttonText}>Yes</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button2} onPress={this.onPress}>
                    <Text style={styles.buttonText2}>No</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffcf33',
    },
    doText: {
        marginVertical: 80,
        fontSize: 30,
        color: 'black',
        textAlign: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: 'white',
        borderRadius: 25,
        marginVertical: 30,
        width: 300,
        paddingVertical: 13,
        marginLeft: 30
    },
    buttonText: {
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 30,
        fontWeight: '500',
        color: 'black'
    },
    button2: {
        backgroundColor: 'white',
        borderRadius: 25,
        marginVertical: 30,
        width: 300,
        paddingVertical: 13,
        marginLeft: 30
    },
    buttonText2: {
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 30,
        fontWeight: '500',
        color: 'black'
    },
});