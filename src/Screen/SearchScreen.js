import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MapView, {Callout, PROVIDER_GOOGLE} from "react-native-maps";
import MenuButton from '../Pages/MenuButton';
import DriverCar from "../Screen/DriverCar";
import CurrentLocationButton from "../Screen/CurrentLocationButton";
import URL from "../Static/URL";
import SearchPlaces from "../Pages/SearchPlaces";



const search_timeout = 1000 * 60 * 10; // 10 minutes
const share_timeout = 1000 * 60 * 5; // 5 minutes

export default class SearchScreen extends React.Component {
    centerMap() {
        const {latitude, longitude, latitudeDelta, longitudeDelta} =this.state.region;
        this.map.animateToRegion({
            latitude,
            longitude,
            latitudeDelta,
            longitudeDelta,
        })
    }
    constructor(props) {
        super(props)
        this.state = {
            car_number: '',
            car_city: '',
            car_lat: '',
            car_long: '',
            car_model: '',
            car_altitude: '',
            car_admin_uuid: '',
            is_moving: '',
            car_type:'',
            car_fuel_type: '',
            car_capacity: '',
            driver_uuid: ''
        }
    }
    UNSAFE_componentWillMount = () => {
       /* fetch('URL.BASE+URL.SHOW_CARS', {
            method: 'GET'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    data: responseJson
                })
            })
            .catch((error) => {
                console.error(error);
            });*/

    }

    componentDidMount = async () => {

        // URL.BASE+URL.SHOW_CARS

        console.log(URL.BASE+URL.SHOW_CARS);


        let GET_AUTH_TOKEN = await fetch(URL.BASE+URL.SHOW_CARS, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              //  'Authorization': 'Bearer '+_TOKEN,
            }
            /*body: JSON.stringify({

                title:this.state.title,
                description:this.state.description,
                star:this.state.star,
                slug:this.state.Detailsdata.create_feed_backs.secure,


            }),*/
        });

        let RECEIVE_AUTH_TOKEN = await GET_AUTH_TOKEN.json();
        console.log(RECEIVE_AUTH_TOKEN);









    }
    OnFeedback= async () => {
        this.setState({loading: true});

        let _TOKEN = await AsyncStorage.getItem('AuthToken');
        let GET_AUTH_TOKEN = await fetch(URL.BASE+URL.ADD_CAR ,{
                method: "post",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },

                body: JSON.stringify({
                    car_number: "wb01pa2256",
                    car_city: "1",
                    car_lat: 26.6958961,
                    car_long: 88.4256909,
                    car_model: "MARUTI",
                    car_altitude: 200,
                    car_admin_uuid: "cd178b30-cbca-11e9-a8fa-b53fbf7a56be",
                    is_moving: "1",
                    car_type: "1",
                    car_fuel_type: "petrol",
                    car_capacity: "10",
                    driver_uuid: "404c09f0-cbe4-11e9-85a6-5f93188e7c5d"

                })
            })
            .then(response => response.json())
            .then(serverResponse => console.warn(serverResponse))

        let RECEIVE_AUTH_TOKEN = await GET_AUTH_TOKEN.json();
        console.log(RECEIVE_AUTH_TOKEN);
        this.setState({loading: false});
        this.props.navigation.navigate('SearchPlaces');

    };

    render() {
        if (this.state.loading === true)  {
            return (<SearchPlaces/>)
        }
        else {
            return (
                <View style={styles.container}>
                    <View style={styles.inputContainer}>
                        <MenuButton onPress={() => {
                            this.props.navigation.navigate('')
                        }}/>

                    </View>
                    <View style={styles.container2}>
                        <MapView
                            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                            style={styles.map}
                            showsUserLocation={true}
                            showsMyLocationButton={false}
                            zoomEnabled={true}
                            currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
                            currentLocationLabel="Current location"
                            nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            GoogleReverseGeocodingQuery={{
                                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            }}
                            ref={(map) => {
                                this.map = map
                            }}
                            initialRegion={{
                                latitude: 26.6906961,
                                longitude: 88.4206709,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}>
                            <MapView.Marker
                                coordinate={{
                                    latitude: 26.6906961,
                                    longitude: 88.4206709,
                                }}
                                title={"Hi"}
                                description={"You are here"}
                            />
                        </MapView>
                        <DriverCar
                            driver={{
                                driver_uuid: 'null', location: {
                                    latitude: 26.6958961,
                                    longitude: 88.4256909,
                                }}}
                        />


                        <CurrentLocationButton cb={() => {
                            this.centerMap()
                        }}/>
                    </View>


                    <View style={styles.footer}/>
                    <TouchableOpacity style={styles.btnFooter} onPress={this.OnFeedback}>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',}}>
                            <Icon style={styles.iconFooter} name="map-search-outline"/>
                            <Text style={styles.footerText}>SEARCH</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }
    }
}
const styles = StyleSheet.create({
    container:{
        flex: 1,

    },

    inputContainer: {
        height: 35,
        width: '100%',
        backgroundColor: '#ffcf33',
    },
    container2: {
        ...StyleSheet.absoluteFillObject,
        top: '6%',
        left: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    footer: {
        flex: 3,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },

    btnFooter: {
        backgroundColor: '#ffcf33',
        padding: 15,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },

    footerText: {
        fontSize: 14,
        fontWeight: 'normal',
        color: 'black',
    },

    iconFooter: {
        display: 'flex',
        fontSize: 20,
        marginRight: 10,
        color: 'black',
        alignItems: 'center',
        justifyContent: "center",
    },
});