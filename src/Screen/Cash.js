import React, { Component } from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import BackButton from "../Pages/BackButton";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";



export default class Cash extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topLinks}>
                    <BackButton navigation={this.props.navigation} />
                    <Text style={styles.caText}>Cash</Text>
                </View>
                    <Icon name="cash" backgroundColor="#3b5998" style={styles.caIcon}/>
                    <Icon name="car" backgroundColor="#3b5998" style={styles.carIcon}/>
                <Text style={styles.paytripText}>Pay for trips in cash</Text>
                <Text style={styles.amountText}>Your driver's phone will show you the amount to pay at the end of the trip.</Text>
                <TouchableOpacity style={styles.button} onPress={this.onPress}>
                    <Text style={styles.nextText}>Next</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    topLinks: {
        height: 80,
        backgroundColor: '#ffcf33'
    },
    caText: {
        fontSize: 30,
        marginLeft: 15
    },
    caIcon: {
        fontSize: 80,
        marginLeft: 130,
        color: 'green'
    },
    carIcon: {
        fontSize: 120,
        marginLeft: 120,
        color: 'blue'
    },
    paytripText: {
        marginVertical: 20,
        fontSize: 20,
        marginLeft: 16
    },
    amountText: {
        marginVertical: 12,
        fontSize: 17,
        marginLeft: 15
    },
    button: {
        backgroundColor: 'black',
        borderRadius: 25,
        marginVertical: 80,
        width: 200,
        paddingVertical: 8,
        marginLeft: 80,

    },
    nextText: {
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 20,
        fontWeight: '500',
        color: 'white'
    },
});