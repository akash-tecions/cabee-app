import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import BackButton from "../Pages/BackButton";

export default class YourTrips extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.above}>
                    <BackButton navigation={this.props.navigation} />
                    <Text style={styles.text}>Your trips</Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    above: {
        height: 80,
        backgroundColor: '#ffcf33',
    },
    text: {
        fontSize:30,
    }
});
