import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, TextInput, Image} from 'react-native';
import Icon from "react-native-vector-icons/index";

export default class Card extends React.Component {
    render() {
        return(
            <View style={styles.bottomContainer}>
            <View style={styles.container}>
                <TouchableOpacity style={styles.card}>
                    <Image style={styles.cardImage} source={require('../Images/cardcar.jpeg')}/>
                </TouchableOpacity>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon name="dots-vertical" size={30} color='black' style={{ paddingTop: 12 }} />
                        <TextInput style={styles.pickUpInput} underlineColorAndroid="transparent" placeholder='Enter Your pickup location' />
                </View>
            </View>
            </View>
        )
    }
}
const styles = StyleSheet.create ({

    container: {
        marginTop: 20,
        backgroundColor: "#f5fcff"
    },
    card: {
        backgroundColor: "#fFF",
        marginBottom: 10,
        marginLeft: '2%',
        width: '96%',
        shadowColor: '#000',
        shadowOpacity: 0.2,
        shadowRadius: 1,
        shadowOffset: {
            width: 3,
            height: 3
        }

    },
    cardImage: {
        width: '100%',
        height: 200,
        resizeMode: 'cover'
    },
    pickUpInput: {
        padding: 8,
        width: '80%',
        margin: 4,
        backgroundColor: 'white',
        borderRadius: 4,
    },

})
