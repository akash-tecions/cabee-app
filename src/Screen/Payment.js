import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import BackButton from "../Pages/BackButton";

export default class Payment extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topLinks}>
                    <BackButton navigation={this.props.navigation} />
                    <Text style={styles.paymentText}>Payment</Text>
                </View>
                <Text style={styles.addText}>Add payment method</Text>
                <View style={styles.line1}>
                </View>
                <Text style={styles.tripText}>Trip profiles</Text>
                <Text style={styles.perText}>Personal</Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    topLinks: {
        height: 80,
        backgroundColor: '#ffcf33',
    },
    paymentText: {
        fontSize:30,
    },
    addText: {
        textAlign: 'left',
        marginLeft: 20,
        color: '#7AD7FD',
        marginVertical:16,
    },
    line1: {
        paddingTop: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#777777'
    },
    tripText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray',
        marginVertical:16,
    },
    perText: {
        textAlign: 'left',
        marginLeft: 30,
        color: 'black',
    }
});
