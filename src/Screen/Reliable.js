import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";

export default class Reliable extends Component {
    render() {
        return (
            <View style={styles.container}>
                    <View style={styles.topLinks}>
                        <Text style={styles.locationText}>Location</Text>
            </View>
                <Text style={styles.devText}>Your device settings</Text>
                <Text style={styles.Text}>Location</Text>
                <Text style={styles.cabText}>Cabee uses your device's location services for more reliable trips</Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    topLinks: {
        height: 50,
        backgroundColor: '#ffcf33'
    },
    locationText: {
        fontSize:30
    },
    devText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray',
        marginVertical:20
    },
    Text: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical: 16,
    },
    cabText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray'
    }
});
