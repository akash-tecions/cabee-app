import React, { Component } from 'react';
import {StyleSheet, View, Text, TouchableOpacity, TextInput} from 'react-native';
import BackButton from "../Pages/BackButton";

export default class AddCard extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topLinks}>
                    <BackButton navigation={this.props.navigation} />
                    <Text style={styles.cardText}>Add Card</Text>
                </View>
                <Text style={styles.numText}>Card number</Text>
                <TextInput style={styles.inputBox}
                           underlinecolorAndroid='black'
                           placeholder="Enter your card number"
                           placeholderTextColor="white"
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    topLinks: {
        height: 80,
        backgroundColor: '#ffcf33'
    },
    cardText: {
        fontSize: 30,
        marginLeft: 15
    },
    numText: {
        marginVertical: 16,
        textAlign: 'left',
        marginLeft: 15,
        color: 'gray',
    },
    inputBox: {
        width: 300,
        backgroundColor: 'gray',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        marginVertical: 16,
        marginLeft: 15
    }
});