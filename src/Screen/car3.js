import React, { Component } from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import {Callout} from "react-native-maps";

export default class Car3 extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text style={styles.driverText}>Driver Found!</Text>
                    <View style={styles.imgView}>
                        <Image style={styles.img} source={require('../Images/account-profile-icon.png')} />
                    </View>
                    <Text style={styles.helloText}>Hello! I am </Text>
                    <Text style={styles.nameText}>Vicky Das</Text>
                    <Text style={styles.namText}>and I am 0.3km away.</Text>
                    <View style={styles.RectangleShape}>
                        <Text style={styles.vehicleText}>Vehicle Plate Number</Text>
                        <Text style={styles.wbText}>WB 03 PA 2257</Text>
                    </View>
                    <TouchableOpacity style={styles.button}
                                      onPress={() => { this.props.navigation.navigate('Some') }}>
                        <Text style={styles.nextText}>Next</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffcf33'
    },
    driverText: {
        marginVertical: 40,
        fontSize: 40,
        color: 'black',
        textAlign: 'center',
        justifyContent: 'center'
    },
    imgView: {
        paddingLeft: 20,
        paddingRight: 20,
        textAlign: 'center',
        marginLeft: 100,
    },
    img: {
        height: 100,
        width: 100,
        borderRadius: 50
    },
    helloText: {
        marginVertical: 16,
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        justifyContent: 'center'
    },
    nameText: {
        marginVertical: 16,
        fontSize: 30,
        color: 'white',
        textAlign: 'center',
        justifyContent: 'center'
    },
    namText: {
        marginVertical: 16,
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        justifyContent: 'center'
    },
    RectangleShape: {
        marginTop: 20,
        width: 120*2,
        height: 120,
        backgroundColor: 'black',
        marginLeft: 60,

    },
    vehicleText: {
        marginVertical: 16,
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        justifyContent: 'center'
    },
    wbText: {
        fontSize: 30,
        color: 'white',
        textAlign: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: 'white',
        borderRadius: 25,
        marginVertical: 30,
        width: 100,
        paddingVertical: 5,
        marginLeft: 130,

    },
    nextText: {
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 20,
        fontWeight: '500',
        color: 'black'
    },
})