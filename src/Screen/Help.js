import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import BackButton from "../Pages/BackButton";

export default class Help extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topLinks}>
                    <BackButton navigation={this.props.navigation} />
                <Text style={styles.helpText}>Help</Text>
                </View>
                <Text style={styles.allText}>All Topics</Text>
                <Text style={styles.triText}>Trip Issues and Refunds</Text>
                <Text style={styles.accText}>Account and Payment Options</Text>
                <Text style={styles.guideText}>A Guide to Cabee</Text>
                <Text style={styles.sigText}>Signing Up</Text>
                <Text style={styles.morText}>More</Text>
                <Text style={styles.accessText}>Accessibility</Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    topLinks: {
        height: 80,
        backgroundColor: '#ffcf33',
    },
    helpText: {
        fontSize:30,
    },
    allText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray',
        marginVertical:16,
        fontSize: 15
    },
    triText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    accText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    guideText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    sigText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    morText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    accessText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    }
});
