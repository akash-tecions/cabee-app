import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import MenuButton from '../Pages/MenuButton';

export default class HomeScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <MenuButton navigation={this.props.navigation} />
                <Text style={styles.text}>Home</Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffcf33'
    },
    text: {
        fontSize:30,
    marginLeft: 15
    }
});
