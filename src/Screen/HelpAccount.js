import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default class Help extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topLinks}>
                    <Text style={styles.helpText}>Help</Text>
                </View>
                <Text style={styles.paText}>Account and Payment Options</Text>
                <Text style={styles.iText}>I can't sign in or request a ride</Text>
                <Text style={styles.changeText}>Changing my account settings</Text>
                <Text style={styles.optText}>Payment options</Text>
                <Text style={styles.usingText}>Using promos and credits</Text>
                <Text style={styles.unkText}>I have an unknown charge</Text>
                <Text style={styles.cText}>Cabee for business travel</Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    topLinks: {
        height: 50,
        backgroundColor: '#ffcf33'
    },
    helpText: {
        fontSize:30,
    },
    paText:{
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 25
    },
    iText:{
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    changeText:{
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    optText:{
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    usingText:{
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    unkText:{
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
    cText:{
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
        marginVertical:16,
        fontSize: 20
    },
});
