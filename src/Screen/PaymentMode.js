import React, { Component } from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import BackButton from "../Pages/BackButton";

export default class PaymentMode extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topLinks}>
                    <BackButton navigation={this.props.navigation} />
                    <Text style={styles.payText}>Add payment method</Text>
                </View>
                <TouchableOpacity  onPress = {() => this.props.navigation}>
                    <Icon name="credit-card" backgroundColor="#3b5998" style={styles.creditIcon}/>
                <Text style={styles.creditText}>Credit or debit card</Text>
                </TouchableOpacity>
                <TouchableOpacity  onPress = {() => this.props.navigation}>
                    <Icon name="cash" backgroundColor="#3b5998" style={styles.cashIcon}/>
                <Text style={styles.cashText}>Cash</Text>
                </TouchableOpacity>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    topLinks: {
        height: 80,
        backgroundColor: '#ffcf33'
    },
    payText: {
       fontSize: 30
    },
    creditIcon: {
        marginVertical: 30,
        fontSize: 30,
        marginLeft: 15,
        flexDirection: 'row',
        color: 'blue'
    },
    creditText: {
        textAlign: 'left',
        marginLeft: 30,
        color: 'black',
        fontSize: 20,
        flexDirection: 'row'
    },
    cashIcon: {
        marginVertical: 30,
        fontSize: 30,
        marginLeft: 15,
        flexDirection: 'row',
        color: 'green'
    },
    cashText: {
        textAlign: 'left',
        marginLeft: 30,
        color: 'black',
        fontSize: 20,
        flexDirection: 'row'
    }
});
