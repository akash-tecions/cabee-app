import React, {Component} from 'react';
import {Button, Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import { withNavigation } from 'react-navigation';



import MapView, {AnimatedRegion, Animated, Marker, PROVIDER_GOOGLE, Callout} from 'react-native-maps';
import URL from "../Static/URL";
import AsyncStorage from "@react-native-community/async-storage";
export default class Booking extends Component {
    constructor(props) {
        super(props)
        this.state = {
            car_id: '',
            driver_uuid: '',
            pickup_lat: '',
            pickup_long: '',
            pickup_address:'',
            drop_lat: '',
            drop_long: '',
            total_distance: '',
            drop_address: '',
        }
        const driver = this.props.driver ?
            this.props.driver :
            {
                uid: "noDriversPassed",
                location: { latitude: 0, longitude: 0 }
            }
        const coordinate = new MapView.AnimatedRegion({
            latitude: driver.location.latitude,
            longitude: driver.location.longitude,
        })
        this.setState({driver: driver,
            coordinate: coordinate,})
    }

    UNSAFE_componentWillMount = () => {

    }

    componentDidMount = async () => {

        // URL.BASE+URL.SHOW_CARS

        console.log(URL.BASE+URL.SHOW_CARS);


        let GET_AUTH_TOKEN = await fetch(URL.BASE+URL.GET_CITY_CARS+1, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                //  'Authorization': 'Bearer '+_TOKEN,
            }
            /*body: JSON.stringify({

                title:this.state.title,
                description:this.state.description,
                star:this.state.star,
                slug:this.state.Detailsdata.create_feed_backs.secure,


            }),*/
        });

        let RECEIVE_AUTH_TOKEN = await GET_AUTH_TOKEN.json();
        console.log(RECEIVE_AUTH_TOKEN);



    }
    OnFeedback= async () => {
        this.setState({loading: true});

        let _TOKEN = await AsyncStorage.getItem('AuthToken');
        let GET_AUTH_TOKEN = await fetch(URL.BASE+URL.ADD_BOOKING_CONFIRM+1 ,{
            method: "post",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },

            body: JSON.stringify({
                car_id: "1",
                driver_uuid: "404c09f0-cbe4-11e9-85a6-5f93188e7c5d",
                pickup_lat:26.6916961,
                pickup_long: 88.4216709,
                pickup_address: "Siliguri",
                drop_lat: 26.6946961,
                drop_long: 88.4446709,
                total_distance: 111,
                drop_address: "Ranidanga",


            })
        })
            .then(response => response.json())
            .then(serverResponse => console.warn(serverResponse))

        let RECEIVE_AUTH_TOKEN = await GET_AUTH_TOKEN.json();
        console.log(RECEIVE_AUTH_TOKEN);
        this.setState({loading: false});

    };

    render() {
        return(
            <View style={styles.container}>
            <View style={styles.inputContainer}>
                <Text style={styles.textContainer}>Click on the car's for booking</Text>
            </View>
                <View style={styles.container2}>

            <MapView
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={ styles.map }
                showsUserLocation={true}
                showsMyLocationButton={false}
                zoomEnabled = {true}
                ref={(map) => {this.map = map}}
                initialRegion={{
                    latitude: 26.6906961,
                    longitude: 88.4206709,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}>
                <MapView.Marker
                    coordinate={{
                        latitude: 26.6916961,
                        longitude: 88.4216709,
                    }}
                    anchor={{x: 0.35, y: 0.32}}
                    ref={marker => { this.marker = marker}}
                    style={{width: 10, height: 10}}
                    image={require('../Images/car2.png')}
                    width={48}
                    height={48}>
                    <Callout style={styles.plainView}
                             onPress={() => { this.props.navigation.navigate('Car1') }}>
                        <View>
                            <Text>WB 01 PA 2256</Text>
                        </View>
                        <Button

                            title="Book Now"
                            color="#000000"
                        />
                    </Callout>
                </MapView.Marker>
                <MapView.Marker
                    coordinate={{
                        latitude: 26.6958961,
                        longitude: 88.4256909,
                    }}
                    anchor={{x: 0.35, y: 0.32}}
                    ref={marker => { this.marker = marker}}
                    style={{width: 10, height: 10}}
                    image={require('../Images/car.png')}
                    width={48}
                    height={25}>
                    <Callout style={styles.plainView}
                             onPress={() => { this.props.navigation.navigate('Car') }}>
                        <View>
                            <Text>WB 02 PA 2255</Text>
                        </View>
                        <Button

                            title="Book Now"
                            color="#000000"
                        />
                    </Callout>
                </MapView.Marker>
            </MapView>
                </View>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex: 1,

    },
    inputContainer: {
        flex: 1,
        height: 35,
        width: '100%',
        backgroundColor: '#ffcf33',
    },
    textContainer: {
        marginVertical: 5,
      fontSize: 20,
        marginLeft: 5
    },
    container2: {
        ...StyleSheet.absoluteFillObject,
        top: '6%',
        left: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    plainView: {
        width: 100,
        backgroundColor: '#ffcf33',
    },

});



