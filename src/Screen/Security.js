import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";
import BackButton from "../Pages/BackButton";

export default class Security extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topLinks}>
                    <BackButton navigation={this.props.navigation} />
                    <Text style={styles.securityText}>Security</Text>
                </View>
                <View style={styles.line1}>
                </View>
                <Text style={styles.stepText}>2-step verification</Text>
                <Text style={styles.unText}>Unavailable</Text>
                <View style={styles.line2}>
                </View>
                <Text style={styles.unfText}>Unfortunately, 2-step verification is unavailable at the moment</Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    topLinks: {
        height: 80,
        backgroundColor: '#ffcf33'
    },
    securityText: {
        fontSize:30
    },
    line1: {
        marginVertical: 16,
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#777777'
    },
    stepText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'black',
    },
    unText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray',
    },
    line2: {
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#777777'
    },
    unfText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray',
        marginVertical:16
    }
});
