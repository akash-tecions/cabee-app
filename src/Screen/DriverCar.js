import React, {Component} from 'react';
import {Button, Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import { withNavigation } from 'react-navigation';



import MapView, {AnimatedRegion, Animated, Marker, PROVIDER_GOOGLE, Callout} from 'react-native-maps';
class DriverCar extends Component {
    constructor(props) {
        super(props);
        const driver = this.props.driver ?
            this.props.driver :
            {
                uid: "noDriversPassed",
                location: { latitude: 0, longitude: 0 }
            }
        const coordinate = new MapView.AnimatedRegion({
            latitude: driver.location.latitude,
            longitude: driver.location.longitude,
        })
        this.setState({driver: driver,
            coordinate: coordinate,})
    }


    render() {
        return(



            <MapView
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={ styles.map }
                showsUserLocation={true}
                showsMyLocationButton={false}
                zoomEnabled = {true}
                ref={(map) => {this.map = map}}
                initialRegion={{
                    latitude: 26.6906961,
                    longitude: 88.4206709,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}>
                <MapView.Marker
                    coordinate={{
                        latitude: 26.6916961,
                        longitude: 88.4216709,
                    }}
                    anchor={{x: 0.35, y: 0.32}}
                    ref={marker => { this.marker = marker}}
                    style={{width: 10, height: 10}}
                    image={require('../Images/car2.png')}
                    width={48}
                    height={48}>
                    <Callout style={styles.plainView}
                             onPress={() => { this.props.navigation.navigate('Car1') }}>
                        <View>
                            <Text>WB 01 PA 2256</Text>
                        </View>
                        <Button

                            title="Book Now"
                            color="#000000"
                        />
                    </Callout>
                </MapView.Marker>
                <MapView.Marker
                    coordinate={{
                        latitude: 26.6958961,
                        longitude: 88.4256909,
                    }}
                    anchor={{x: 0.35, y: 0.32}}
                    ref={marker => { this.marker = marker}}
                    style={{width: 10, height: 10}}
                    image={require('../Images/car.png')}
                    width={48}
                    height={25}>
                <Callout style={styles.plainView}
                    onPress={() => { this.props.navigation.navigate('Car') }}>
                    <View>
                        <Text>WB 02 PA 2255</Text>
                    </View>
                    <Button

                        title="Book Now"
                        color="#000000"
                    />
                </Callout>
                </MapView.Marker>
            </MapView>

        )
    }
}
const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    plainView: {
        width: 100,
        backgroundColor: '#ffcf33',
    },

});


export default withNavigation(DriverCar);
