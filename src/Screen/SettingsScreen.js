import React, { Component } from "react";
import {StyleSheet, View, Text, Image, TouchableOpacity} from "react-native";

import { HeaderBackButton } from "react-navigation";
import BackButton from "../Pages/BackButton";
import MenuButton from "../Pages/MenuButton";

export default class SettingsScreen extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (<HeaderBackButton onPress={() => {
                navigation.navigate('HomeScreen')
            }}/>)
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.above}>
                    <BackButton navigation={this.props.navigation} />
                <Text style={styles.text}>Settings</Text>
                </View>
                <View style={styles.topLinks}>
                    <View style={styles.profile}>
                        <View style={styles.imgView}>
                            <Image style={styles.img} source={require('../Images/account-profile-icon.png')} />
                        </View>
                        <View style={styles.profileText}>
                            <Text style={styles.name}>User Name</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.bottomLinks}>
                    <Text style={styles.favText}>Favourites</Text>
                    <Text style={styles.homeText}>Add Home</Text>
                    <Text style={styles.workText}>Add Work</Text>
                    <Text style={styles.moreText}>More saved places</Text>
                    <View style={styles.line}>
                    </View>
                    <Text style={styles.priText}>Privacy Settings</Text>
                    <Text style={styles.manText}>Manage the data you share with us</Text>
                    <View style={styles.line2}>
                    </View>
                    <Text style={styles.secText}>Security</Text>
                    <Text style={styles.conText}>Control your account security with 2-steps verification</Text>
                    <View style={styles.line3}>
                    </View>
                    <Text style={styles.signText}>Sign Out</Text>
                </View>
                </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffcf33',
    },
    above: {
        height: 80,
        backgroundColor: '#ffcf33',
        marginLeft: 15,
    },
    text: {
        fontSize:30,
    },
    profile: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#777777',
    },
    profileText: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    name: {
        fontSize: 20,
        paddingBottom: 5,
        color: 'black',
        textAlign: 'left'
    },
    imgView: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20
    },
    img: {
        height: 50,
        width: 50,
        borderRadius: 50,
    },
    topLinks: {
        height: 100,
        backgroundColor: 'white'
    },
    bottomLinks: {
        flex: 1,
        backgroundColor: 'white'
    },
    favText: {
        textAlign: 'left',
        marginLeft: 20,
        color: 'gray',
        marginVertical: 16
    },
    homeText: {
        flex: 1,
        textAlign: 'left',
        marginLeft: 30,
        color: 'black'
    },
    workText: {
        flex: 1,
        textAlign: 'left',
        marginLeft: 30,
        color: 'black',
    },
    moreText: {
        textAlign: 'left',
        marginLeft: 20,
        color: '#7AD7FD',
    },
    line: {
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#777777'
    },
    priText: {
        textAlign: 'left',
        marginLeft: 30,
        color: 'black',
        marginVertical: 16
    },
    manText: {
        textAlign: 'left',
        marginLeft: 30,
        color: 'gray',
    },
    line2:{
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#777777'
    },
    secText: {
        textAlign: 'left',
        marginLeft: 30,
        color: 'black',
        marginVertical: 16
    },
    conText: {
        textAlign: 'left',
        marginLeft: 30,
        color: 'gray',
    },
    line3: {
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#777777'
    },
    signText: {
        flex: 1,
        textAlign: 'left',
        marginLeft: 30,
        color: 'black'
    }
});
