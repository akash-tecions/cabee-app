import React from 'react';
import {Image, Text, TextInput, View, StyleSheet} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { AppRegistry } from 'react-native';
import Icon from "react-native-vector-icons/index";
export default class Places2 extends React.Component {
    render() {
        return (

            <GooglePlacesAutocomplete
                placeholder='Enter your dropoff location'
                minLength={2}
                autoFocus={false}
                returnKeyType={'search'}
                keyboardAppearance={'light'}
                listViewDisplayed={false}
                fetchDetails={true}
                keyboardShouldPersistTaps="handled"
                renderDescription={row => row.description}
                onPress={(data, details = null) => {
                    console.log(data, details);
                    this.setState({
                        location: data.structured_formatting.main_text,
                        user_long: details.geometry.location.lng,
                        user_lat: details.geometry.location.lat
                    })
                }}
                enablePoweredByContainer={false}
                getDefaultValue={() => ''}
                textInputProps={{
                    ref: (input) => {
                        this.fourthTextInput = input
                    }
                }}
                query={{
                    // available options: https://developers.google.com/places/web-service/autocomplete
                    key: 'AIzaSyD2NGQ9tznUjXaIVX5RAASX-6SLq3bgSJk',
                    language: 'en',
                    region: "WEST BENGAL", //It removes the country name from the suggestion list
                    types: 'geocode', // default: 'geocode'
                    components: ''
                }}
                styles={{
                    textInputContainer: {
                        marginVertical: 50,
                        marginLeft:30,
                        width: '80%',
                        padding: 8,
                        margin: 4,
                        backgroundColor: 'white',

                    },
                    description: {
                        fontWeight: 'bold'
                    },
                    predefinedPlacesDescription: {
                        color: '#1faadb'
                    }
                }}
                currentLocation={false}
                nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                GoogleReverseGeocodingQuery={{// available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                }}
                GooglePlacesSearchQuery={{
                    // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                    rankby: 'distance',
                    type: 'cafe'
                }}
                GooglePlacesDetailsQuery={{
                    // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                    fields: 'formatted_address',
                }}
                filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
                debounce={200}
            />
        );
    }
}
