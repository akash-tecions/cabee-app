
import React, {Component} from 'react';
import {
    Linking,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from "react-native";
import Geocoder from 'react-native-geocoding';
export default class SearchRoutes extends Component {
    constructor() {
        super();
        this.state = {
            location: null
        };
    }

    getData() {
        Geocoder.init("AIzaSyD2NGQ9tznUjXaIVX5RAASX-6SLq3bgSJk"); // use a valid API key


        Geocoder.from(41.89, 12.49)
            .then(json => {
                const addressComponent = json.results[0].address_components[0];
                console.log(addressComponent);
            })
            .catch(error => console.log(error));
    }
}