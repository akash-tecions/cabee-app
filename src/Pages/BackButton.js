import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class BackButton extends Component {
    render() {
        return (
            <View>
                <TouchableOpacity  onPress = {() => this.props.navigation.navigate('nav')}>
                <Icon name="arrow-back" backgroundColor="#3b5998" style={styles.menuIcon}/>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginVertical:16,
        backgroundColor: '#DDDDDD',
    },
    menuIcon: {
        fontSize: 30,
        marginLeft: 15,
    }
});