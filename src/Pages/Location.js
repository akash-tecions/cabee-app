import React, {Component} from 'react';
import {
    StyleSheet,
    Text, StatusBar,
    View, ScrollView, Dimensions
} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
const {width, height} = Dimensions.get('window');


export default class Location extends Component<{}> {
    static navigationOptions = {
        title: 'Select Address',
        headerStyle: {
            backgroundColor: '#102b41',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'normal',
        },
// headerLeft: null
    };

    constructor(props) {
        super(props)
        this.state = {
            locationdata: 'Siliguri, West Bengal, India'
        }
    }

    submiotdfd = (value, data, details) => {
        console.log(value)
        console.log(data.geometry)
        console.log(details)
        this.setState({

            locationdata: value.description
        })

    }


    render() {

        //this.props.navigation.state.params.returnData(this.state.locationdata)
        return (
            <View style={styles.container}>

                <View style={{height: height - 120}}>

                    <GooglePlacesAutocomplete
                        placeholder='Your Location'
                        minLength={2}
                        autoFocus={false}
                        returnKeyType={'default'}
                        fetchDetails={true}
                        value={this.props.endGoogle}
                        onPress={this.submiotdfd
                        }

                        currentLocationLabel="Current location"
                        nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch


                        query={{
// available options: https://developers.google.com/places/web-service/autocomplete
                            key: 'AIzaSyDrAu8pBDU8v6rxG-WlX7Roc0wWNQa9w0Q',
// types: '(geocode,cities)' // default: 'geocode'
                        }}
                        styles={{
                            container: {
                                height: height - width,
// position: 'absolute',
                                margin: 40,
                                width: width - 50,
// top:250,
                            },
                            textInputContainer: {
                                width: width - 50,
                                paddingLeft: 0,
                                paddingRight: 0,
                                backgroundColor: 'rgba(0,0,0,0)',
                                borderTopWidth: 0,
                                borderBottomWidth: 0
                            },

                            textInput: {
                                width: width - 50,
                                padding: 10,
                                marginLeft: 0,
                                marginRight: 0,
                                height: 38,
                                color: '#5d5d5d',
                                fontSize: 16
                            },
                            listView: {
                                width: width - 50,
// paddingLeft:10,
// padding:10,
                                elevation: 10,
                                backgroundColor: '#ffffff'
                            },
                            powered: {
                                display: 'none'
                            }

                        }}
                        currentLocation={true}
                    />

                    <Text>{this.state.locationdata}</Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        height: height,
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },

});
