import React from 'react';
import {Image, Text, TextInput, View, StyleSheet} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { AppRegistry } from 'react-native';
import Icon from "react-native-vector-icons/index";
export default class Places extends React.Component {
    render() {
        return (

                <GooglePlacesAutocomplete
                    placeholder='Enter your pickup location'
                    minLength={2}
                    autoFocus={false}
                    returnKeyType={'search'}
                    keyboardAppearance={'light'}
                    listViewDisplayed={false}
                    fetchDetails={true}
                    keyboardShouldPersistTaps="handled"
                    renderDescription={row => row.description}
                    onPress={(data, details = null) => {
                        console.log(data, details);
                        this.setState({
                            latitude: details.geometry.location.lat,
                            longitude: details.geometry.location.lng,
                            moveToUserLocation: true
                        });
                    }
                    }
                    enablePoweredByContainer={false}
                    getDefaultValue={() => ''}
                    textInputProps={{
                        ref: (input) => {
                            this.fourthTextInput = input
                        }
                    }}
                    query={{
                        // available options: https://developers.google.com/places/web-service/autocomplete
                        key: 'AIzaSyD2NGQ9tznUjXaIVX5RAASX-6SLq3bgSJk',
                        language: 'en',
                        region: "WEST BENGAL", //It removes the country name from the suggestion list
                        types: 'geocode', // default: 'geocode'
                        components: ''
                    }}
                    styles={{
                        textInputContainer: {
                            marginVertical: 15,
                            marginLeft:30,
                            width: '80%',
                            padding: 8,
                            margin: 4,
                            backgroundColor: 'white',
                            borderRadius: 4,
                        },
                        description: {
                            fontWeight: 'bold'
                        },
                        predefinedPlacesDescription: {
                            color: '#1faadb'
                        }
                    }}
                    currentLocation={false}
                    nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                    GoogleReverseGeocodingQuery={{// available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                    }}
                    GooglePlacesSearchQuery={{
                        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                        rankby: 'distance',
                        type: 'cafe'
                    }}
                    GooglePlacesDetailsQuery={{
                        // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                        fields: 'formatted_address',
                    }}
                    filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
                    debounce={200}
                />
        );
    }
}
