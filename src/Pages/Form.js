import React from "react";
import { View, TextInput, Text } from "react-native";
import Map from "../page/Map";
export default class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loc: ""
        };
    }
    /// state value updated from child component map;
    handler(arg) {
        this.setState({
            loc: arg
        });
        return;
    }
    render() {
        return (
            <View>
                <View>
                    <Text>Name</Text>
                    <TextInput />
                </View>
                <View>
                    <Text>Address</Text>
                    <TextInput />
                </View>
                <View>
                    <Text>Description</Text>
                    <TextInput />
                </View>
                <View>
                    <Text>Location Search</Text>
                    <Text style={{ fontWeight: "bold" }}> {this.state.loc}</Text>
                </View>
                <View>
                    <Map handler={this.handler.bind(this)} />
                </View>
            </View>
        );
    }
}
