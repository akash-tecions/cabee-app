import React, {Component} from 'react';
import {
    AppRegistry, Text, View, Dimensions, ImageBackground, KeyboardAvoidingView, StyleSheet,
    TextInput, TouchableOpacity, keyboardVerticalOffset, Image, TouchableWithoutFeedback, Keyboard, Button
} from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";
import URL from "../Static/URL";

let HEIGHT = Dimensions.get('screen').height;

export default class OTPverification extends Component{
    constructor(props) {
        super(props)
        this.state = {
            temp_phone: '',
            otpEntered:'',
            otpEntered2:'',
            otpEntered3:'',
            otpEntered4:'',
            isLoading: false,
            buttonText: true,

        }
    }

    static navigationOptions = {
        header: null
    };


    componentWillMount() {
        const { navigation } = this.props;
        const temp_phone = navigation.getParam('temp_phone');

        this.setState({
            temp_phone : temp_phone
        });

        console.log(temp_phone)
    }

    async onPressConfirmText() {


        let OTPENTERED = this.state.otpEntered+this.state.otpEntered2+this.state.otpEntered3+this.state.otpEntered4


        let GET_AUTH_TOKEN = await fetch(URL.BASE+URL.CUSTOMER_OTP_VERIFY, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',

            },
            body: JSON.stringify({
                temp_phone: this.state.temp_phone,
                otpEntered: OTPENTERED
            }),
        })

        let data = await GET_AUTH_TOKEN.json();

        console.log(data);
        console.log(OTPENTERED);
        console.log(this.state.temp_phone);
        console.log(data);

        let RECEIVE_AUTH_TOKEN = await GET_AUTH_TOKEN.json();
        AsyncStorage.setItem('AuthToken', RECEIVE_AUTH_TOKEN.access_token);

        let response = await fetch(URL.BASE + URL.PROFILE, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization' : 'Bearer ' + RECEIVE_AUTH_TOKEN.access_token,
            },
        });
        let Data = await response.json();
        console.log(Data);
    }
    render(){

        const { navigation } = this.props;
        const temp_phone = navigation.getParam('temp_phone');





        return(
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <ImageBackground source={require('../Images/back.png')}
                                 style={{ height: HEIGHT,position: 'absolute',
                                     top: 0, left: 0,
                                     right: 0, bottom: 0,
                                     justifyContent: 'center',
                                     alignItems: 'center'}}>
                    <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={50} style={styles.container}>
                        <View style={{justifyContent:'center',alignItems:'center',marginTop:-50}}>
                            <Image style={styles.logo} source={require('../Images/cabee-logo.png')}></Image>
                        </View>
                        <Text style={{color:'white',fontFamily:'montserrat_semibold',fontSize:20,
                            textAlign:"center",marginBottom:20}}>Enter Verification Code for {temp_phone}</Text>
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Image style={styles.envelope} source={require('../Images/envelopecab.png')}></Image>
                        </View>
                        <Text style={{color:'white',fontFamily:'montserrat_semibold',fontSize:20,
                            textAlign:"center"}}>Enter OTP</Text>
                        <Text style={{color:'white',fontSize:18,textAlign:"center",fontFamily:'montserrat_semibold'}}>We have send an OTP to your Phone</Text>
                        <View style={{justifyContent:'center',alignItems:'center',marginTop:40,borderColor:'#fff',
                            borderRadius:10,backgroundColor:'rgba(255,255,255,0.6)',borderWidth:5,padding:20}}>
                            <View style={{flexDirection:'row',justifyContent:'center',marginTop:40}}>
                                <TextInput
                                    keyboardType='numeric'
                                    maxLength={1}
                                    style={styles.otpInput}
                                    onChangeText={(otpEntered) => this.setState({otpEntered})}/>
                                <TextInput
                                    keyboardType='numeric'
                                    maxLength={1}
                                    style={styles.otpInput}
                                    onChangeText={(otpEntered2) => this.setState({otpEntered2})}/>
                                <TextInput
                                    keyboardType='numeric'
                                    maxLength={1}
                                    style={styles.otpInput}
                                    onChangeText={(otpEntered3) => this.setState({otpEntered3})}/>
                                <TextInput
                                    keyboardType='numeric'
                                    maxLength={1}
                                    style={styles.otpInput}
                                    onChangeText={(otpEntered4) => this.setState({otpEntered4})}/>
                            </View>

                            <View style={{flexDirection:'row'}}>
                                <Text style={{color:'#1b5e85',fontFamily:'montserrat_semibold'}}>Did not recieve OTP?</Text><TouchableOpacity>
                                <Text style={{fontFamily:'montserrat_semibold',color:'#1b5e85'}}>Resend?</Text></TouchableOpacity>
                            </View>
                            <TouchableOpacity style={styles.buttonContainer}
                                              onPress={() =>  this.props.navigation.navigate('SearchScreen')}>
                                <Text style={styles.buttonText} onPress={this.onPressConfirmText.bind(this)}>CONFIRM</Text></TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>

                </ImageBackground>
            </TouchableWithoutFeedback>
        );
    }
}
const styles= StyleSheet.create({
    buttonContainer:{
        backgroundColor:'black',
        paddingVertical:10,
        marginTop:10,
        width:250,
        borderRadius:100
    },
    buttonText:{
        textAlign:'center',
        color:'#fff',
        fontFamily:'montserrat_semibold',
        fontSize:17
    },
    envelope:{
        width:70,
        height:70,
    },
    logo:{
        width:160,
        height:90,
    },
    otpInput:{
        height:50,width:50,margin:5,borderRadius:10,color:'#fff',fontFamily:'montserrat_semibold',fontSize:20,
        backgroundColor:'#FFD700',textAlign:'center',padding:5,borderColor:'#FFD700',elevation:5,borderWidth:3
    }
});