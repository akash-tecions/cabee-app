import React, {Component} from 'react';
import {Text, View,Image,ImageBackground,KeyboardAvoidingView,
    Dimensions,TouchableOpacity,StyleSheet,TextInput,TouchableWithoutFeedback,Keyboard} from 'react-native';

import URL from "../Static/URL";
import AsyncStorage from "@react-native-community/async-storage";


let HEIGHT = Dimensions.get('screen').height;

export default class OTPconfirmation extends Component{
    constructor(props) {
        super(props)
        this.state = {
            temp_phone: " ",

        }
    }

    static navigationOptions = {
        header: null
    };
    componentWillMount() {

    }

    async onPressSendOtpText() {
        let GET_AUTH_TOKEN = await fetch(URL.BASE+URL.REGISTER_CUSTOMER, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',

            },
            body: JSON.stringify({

                temp_phone: this.state.temp_phone
            }),
        });

        //	console.log(USERNAME);

        let RECEIVE_AUTH_TOKEN = await GET_AUTH_TOKEN.json();
        console.log(RECEIVE_AUTH_TOKEN);
        this.props.navigation.navigate('OTPverification', {temp_phone: this.state.temp_phone});



















        // const data = {
        //     temp_phone: this.state.temp_phone,
        // };
        // try {
        //     let response = await fetch(
        //         URL.BASE+URL.REGISTER_CUSTOMER,
        //         {
        //             method: "POST",
        //             headers: {
        //                 "Accept": "application/json",
        //                 "Content-Type": "application/json"
        //             },
        //             body: JSON.stringify(data)
        //         }
        //     )
        //         .then((response) => { return  response.json() } )
        //         .catch((error) => console.warn("fetch error:", error))
        //         .then((response) => {
        //             console.log(JSON.stringify(response));
        //         })
        //     if (response.status >= 200 && response.status < 300) {
        //         alert("authenticated successfully!!!");
        //     }
        // } catch (errors) {
        //
        //     alert(errors);
        // }
    }

    render(){




        return(
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <ImageBackground source={require('../Images/back.png')}
                                 style={{ height: HEIGHT,position: 'absolute',
                                     top: 0, left: 0,
                                     right: 0, bottom: 0,
                                     justifyContent: 'center',
                                     alignItems: 'center'}}>
                    <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={50} style={styles.container}>
                        <View style={{justifyContent:'center',alignItems:'center',marginTop:-50}}>
                            <Image style={styles.logo} source={require('../Images/cabee-logo.png')}></Image>
                        </View>
                        <Text style={{color:'white',fontSize:20,
                            textAlign:"center",marginBottom:20,fontFamily:'montserrat_semibold'}}>Enter Verification Code</Text>
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Image style={styles.envelope} source={require('../Images/envelopecab.png')}></Image>
                        </View>
                        <Text style={{color:'white',fontFamily:'montserrat_semibold',fontSize:20,
                            textAlign:"center"}}>Enter your Mobile Number</Text>
                        <Text style={{color:'white',fontSize:18,textAlign:"center",fontFamily:'montserrat_semibold'}}>We will send you an OTP</Text>
                        <View style={{justifyContent:'center',alignItems:'center',marginTop:40,borderColor:'#fff',
                            borderRadius:10,backgroundColor:'rgba(255,255,255,0.6)',borderWidth:5,padding:20}}>
                            <TextInput
                                style={{height: 40,width:250,margin:5,borderBottomColor:'white',borderBottomWidth:1,fontFamily:'montserrat_semibold'}}
                                placeholder="+ (91) India"
                                onChangeText={(text) => this.setState({text})}/>
                            <TextInput
                                keyboardType='numeric'
                                style={{height: 40,width:250,margin:5,borderBottomColor:'white',borderBottomWidth:1,fontFamily:'montserrat_semibold'}}
                                placeholder="Enter your Mobile Number"
                                onChangeText={(temp_phone) => this.setState({temp_phone})}/>
                            <TouchableOpacity style={styles.buttonContainer}
                                              onPress={() =>  this.props.navigation.navigate('OTPverification')}>
                                <Text style={styles.buttonText} onPress={this.onPressSendOtpText.bind(this)}>SEND OTP</Text></TouchableOpacity>

                        </View>
                    </KeyboardAvoidingView>
                </ImageBackground>
            </TouchableWithoutFeedback>
        );
    }
}
const styles= StyleSheet.create({
    buttonContainer:{
        backgroundColor:'black',
        paddingVertical:10,
        marginTop:10,
        width:250,
        borderRadius:100
    },
    buttonText:{
        textAlign:'center',
        color:'#fff',
        fontFamily:'montserrat_semibold',
        fontSize:17
    },
    envelope:{
        width:70,
        height:70,
    },
    logo:{
        width:160,
        height:90,
    },
});