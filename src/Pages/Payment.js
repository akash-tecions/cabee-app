import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, TextInput, Image} from 'react-native';




export default class Payment extends React.Component {
    render( ){
        return(
            <View style={styles.container}>
                <Image style={styles.logo} source={require('../Images/Tick.png')}></Image>

                <Text style={styles.Text1}>Payment Successful!</Text>
                <Text style={styles.Text2}>Hello Sneha Dhar, Thanks for the payment.</Text>
                <Text style={styles.Text3}>You have paid Rs.1000.00</Text>
                <Text style={styles.Text4}>Your Payment id is MOJO9902Z05A29852362</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#ffcf33'
    },
    logo: {
        marginVertical: 30,
        marginLeft: 130,
        width:160,
        height:160,
    },
    Text1: {
        marginLeft: 40,
        fontSize: 30,
        color: 'blue'
    },
    Text2: {
        marginVertical: 20,
        marginLeft: 20,
        fontSize: 20
    },
    Text3: {
        marginLeft: 40,
        fontSize: 22
    },
    Text4: {
        marginLeft: 30,
        marginVertical: 20,
        fontSize: 22,
    },
})