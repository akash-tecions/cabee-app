import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Places from "./Places";
import Places2 from "./Places2";
import { withNavigation } from 'react-navigation';
import AsyncStorage from "@react-native-community/async-storage";



class SearchPlaces extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            customer_id: " ",
            origin_lat: " ",
            origin_long:" ",
            destination_lat: " ",
            destination_long:  " ",
        }
    }
    static navigationOptions = {
        header: null
    };
    async onPressSearchText()  {
    let GET_AUTH_TOKEN = await fetch(URL.BASE+URL.POST_SEARCH_DATA, {
            method: "post",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },

            body: JSON.stringify({
                customer_id: this.customer_id,
                origin_lat:this.origin_lat,
                origin_long:this.origin_long ,
                destination_lat:this.destination_lat ,
                destination_long:this.destination_long

            })
        });
        let RECEIVE_AUTH_TOKEN = await GET_AUTH_TOKEN.json();
        console.log(RECEIVE_AUTH_TOKEN);
        this.props.navigation.navigate('Booking');

    };


    render() {
        return (
            <View style={styles.container}>
            <View style={styles.inputContainer}>

                <View style={{ flexDirection: 'row' }}>
                    <Icon name="dots-vertical" size={30} color='black' style={{ paddingTop: 15 }} />
                    <Places/>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Icon name="dots-vertical" size={30} color='black' style={{ paddingTop: 50 }} />
                    <Places2/>
                </View>

            </View>
                <View style={styles.footer} />
                <TouchableOpacity style={styles.btnFooter} onPress={this.onPressSearchText.bind(this)}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                        <Icon style={styles.iconFooter} name="map-search-outline" />
                        <Text style={styles.footerText}
                              onPress={() => { this.props.navigation.navigate('Booking') }}>SEARCH</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#ffcf33',

    },

    inputContainer: {
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffcf33',
    },
    footer: {
        flex: 3,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },

    btnFooter: {
        backgroundColor: 'black',
        padding: 15,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },

    footerText: {
        fontSize: 14,
        fontWeight: 'normal',
        color: 'white',
    },

    iconFooter: {
        display: 'flex',
        fontSize: 20,
        marginRight: 10,
        color: 'white',
        alignItems: 'center',
        justifyContent: "center",
    },
});
export default withNavigation(SearchPlaces);