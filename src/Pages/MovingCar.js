import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Platform,
} from 'react-native';
import CurrentLocationButton from "../Screen/CurrentLocationButton";

import MapView, {
    ProviderPropType,
    Marker,
    AnimatedRegion,
} from 'react-native-maps';


const screen = Dimensions.get('window');

const ASPECT_RATIO = screen.width / screen.height;
export default class MovingCar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            latitude: 26.6997061,
            longitude: 88.4297709,
            angle: 0
        }
    }

    async componentDidMount(){
        await navigator.geolocation.getCurrentPosition(
            (position) => {
                let latitude = position.coords.latitude;
                let longitude = position.coords.longitude;
                let angle = position.coords.heading;
                this.setState({latitude, longitude, angle});
            },

            error => console.log(error),

            {enableHighAccuracy: true}

        );

        navigator.geolocation.watchPosition(
            (position) => {
                let latitude = position.coords.latitude;
                let longitude = position.coords.longitude;
                let angle = position.coords.heading;
                this.setState({latitude, longitude, angle});
            },
            error => console.log(error),
            {
                 enableHighAccuracy: true
            }
        );
    }


    getMapRegion() {
        return {
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: 0.9271,
            longitudeDelta: ASPECT_RATIO * 0.9271
        }
    }

    render() {
        let angle = this.state.angle || 0;
        return (
            <View style={styles.container2}>
            <MapView style={styles.map}
                     showUserLocation={true}
                     currentLocation={true}
                     showsMyLocationButton={false}
                     zoomEnabled = {true}
                     currentLocationLabel="Current location"
                     nearbyPlacesAPI='GooglePlacesSearch'
                     followUserLocation={true}
                     region={this.getMapRegion()}>
                <MapView.Marker
                    title={'Hi'}
                    style={{
                        transform: [
                            { perspective: 850 },
                            { translateX: - Dimensions.get('window').width * 0.24 },
                            { rotateY: '60deg'},

                        ],
                    }}

                    image={require('../Images/car3.png')}
                    coordinate={{latitude: this.state.latitude, longitude: this.state.longitude}}/>
            </MapView>
                <CurrentLocationButton cb={() => {this.centerMap()}}/>
            </View>
        );
    }
}
MovingCar.propTypes = {
    provider: ProviderPropType,
};
const styles = StyleSheet.create({
    container2: {
        ...StyleSheet.absoluteFillObject,
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});

