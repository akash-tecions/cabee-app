import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import { withNavigation } from 'react-navigation';

class MenuButton extends Component {
    render() {
        return (
            <View style={styles.header}>
                <TouchableOpacity  onPress = {() => this.props.navigation.navigate('MenuDrawer')}>
                    <Icon name="bars" backgroundColor="#3b5998" style={styles.menuIcon}/>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    header:{
        flex: 1,
        flexDirection: 'row',
        marginLeft: 5,
        paddingHorizontal: 18,
        paddingTop: 5,

    },
    menuIcon: {
        fontSize: 30,
        marginLeft: 15,
    }
});
export default withNavigation(MenuButton);
