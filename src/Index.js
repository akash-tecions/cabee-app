import React from "react";
import {View, Text, Dimensions} from "react-native";
import {createStackNavigator, createAppContainer, createDrawerNavigator} from "react-navigation";

import Form from "./Component/Form";
import OTPconfirmation from "./Pages/OTPconfirmation";
import OTPverification from "./Pages/OTPverification";
//import Map from "./page/Map";
import SearchScreen from "./Screen/SearchScreen";
import MenuDrawer from "./Component/MenuDrawer";
import Car from "./Screen/Car";
import SearchPlaces from "./Pages/SearchPlaces";
import GradientPolylines from "./Pages/GradientPolylines";


import DriverCar from "./Screen/DriverCar";
import Booking from "./Screen/Booking";
import car1 from "./Screen/car1";

import MovingCar from "./Pages/MovingCar";
import YourTrips from "./Screen/YourTrips";
import Payment from "./Pages/Payment";



const Routes = createStackNavigator({
    Form: {
        screen: Form,
    },
    OTPconfirmation: {
        screen: OTPconfirmation,
    },
    OTPverification: {
        screen: OTPverification,
    },
    SearchScreen: {
        screen: SearchScreen,
    },
    MenuDrawer: {
        screen: MenuDrawer,
    },
    SearchPlaces: {
        screen: SearchPlaces,
    },
    Booking: {
        screen:Booking,
    },
    car1: {
        screen: car1,
    },
    Car: {
        screen: Car
    },
    GradientPolylines: {
        screen: GradientPolylines,
    },
    DriverCar: {
        screen: DriverCar,
    },
    YourTrips: {
        screen: YourTrips
    },
    Payment: {
        screen: Payment,
    },
    },
);
export default createAppContainer(Routes);








