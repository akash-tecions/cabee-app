const URL = {
// Base URL
    BASE: 'https://cabee.ml',
    // Login Using Token
    LOGIN: '/oauth/token',
    // Register (POST)
    REGISTER_CUSTOMER: '/api/customer/register',
    //REGISTER_DRIVER: '/api/driver/register',
    //REGISTER_CARADMIN: '/api/caradmin/register',
    //REGISTER_ADMIN: '/api/admin/register',

    // Send OTP (POST)
    //CUSTOMER_OTP_REQUEST: '/api/customer/sendotp',
    //ADMIN_OTP_REQUEST1: '/api/admin/sendotp',
    //CARADMIN_OTP_REQUEST1: '/api/caradmin/sendotp',
    //DRIVER_OTP_REQUEST1: '/api/driver/sendotp',

    // Verify OTP (POST)
    CUSTOMER_OTP_VERIFY: '/api/customer/verify-otp',
    //ADMIN_OTP_VERIFY: '/api/admin/otpverify',
    //CARADMIN_OTP_VERIFY: '/api/caradmin/otpverify',
    //DRIVER_OTP_VERIFY: '/api/driver/otpverify',

    // Send Email Verification Link (POST)
    //CUSTOMER_EMAIL_VERIFIY: '/api/customer/email-verify',
    //ADMIN_OTP_REQUEST2: '/api/admin/email-verify',
    //CARADMIN_OTP_REQUEST2: '/api/caradmin/email-verify',
    //DRIVER_OTP_REQUEST2: '/api/driver/email-verify',

    // Add Car (POST)
    ADD_CAR: '/api/addcar',
    //Show All Cars (GET)
    SHOW_CARS:'/api/getCars',
    // Add Location (POST)
    //ADD_LOCATION: '/api/addlocation',
    // Show Locations (GET)
    //SHOW_LOCATIONS: '/api/showlocations',
    // Get All cars in a City (GET)
    GET_CITY_CARS: '/api/getCityCars/',
    //Post search data
    POST_SEARCH_DATA: '/api/search',
    ADD_TRIP: '/api/addtripdriver',
    ADD_BOOKING_CONFIRM: '/api/bookingconfirm/',

};
export default URL;
