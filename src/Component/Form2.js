import React, { Component, createRef } from 'react';
import CodeInput from 'react-native-confirmation-code-field';
import {StyleSheet, View, Text} from 'react-native';


export default class Form2 extends Component {
     handlerOnFulfill = code => {
         if (isValidCode(code)) {
             this.pasteCode(code);
             console.log(code);
         } else {
             this.clearCode();
         }
     };

     field = createRef();

     clearCode() {
         const {current} = this.field;

         if (current) {
             current.clear();
         }
     }

     pasteCode() {
         const {current} = this.field;

         if (current) {
             current.handlerOnTextChange(value);
         }
     }
      render() {
         return <CodeInput style={styles.container} ref={this.field} onFulfill={this.handlerOnFulfill}/>;
      }
 }
 const styles = StyleSheet.create({
    container : {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da'
    }
});
