import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image
} from 'react-native';

export default class Logo extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={{width:140, height: 90}}
                       source={require('../Images/cabee-logo.png')}/>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});