// export default class HomeScreen extends React.Component {
//     static navigationOptions = {
//         title: 'Welcome',
//     };
//     render() {
//         const {navigate} = this.props.navigation;
//         return (
//             <Button
//                 title="Go to Jane's profile"
//                 onPress={() => navigate('Profile', {name: 'Jane'})}
//             />
//         );
//     }
// }



import React, { Component } from 'react';
import {View, Text, Button} from 'react-native';
export default class Timer extends Component {
    constructor(props: Object) {
        super(props);
        this.state = {timer: 15}
    }

    componentDidMount() {
        this.interval = setInterval(
            () => this.setState((prevState) => ({timer: prevState.timer - 1})),
            1000
        );
    }

    componentDidUpdate() {
        if (this.state.timer === 1) {
            clearInterval(this.interval);
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <View style={{flex: 2, justifyContent: 'center'}}>
                <Text> {this.state.timer} </Text>
                <Button
                    onPress={() =>  this.props.navigation.navigate('DrawerNavigator')}
                    title="Next"
                    color="#000000"
                />
            </View>
        )
    }
}