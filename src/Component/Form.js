
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Button,
    Image, TouchableOpacity
} from 'react-native';


export default class Logo extends Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={{justifyContent:'center',alignItems:'center',marginTop:-50}}>
                    <Image style={styles.logo} source={require('../Images/cabee-logo.png')}></Image>
                </View>
                <Text style={styles.get}>Get moving with Cabee</Text>
                <View>
                    <Button
                        onPress={() =>  this.props.navigation.navigate('OTPconfirmation')}
                        title="Let's Go"
                        color="#000000"
                    />
                </View>
                </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffcf33',
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo:{
        width:160,
        height:90,
    },
    get: {
        marginVertical: 16,
        fontSize: 25
    },

});